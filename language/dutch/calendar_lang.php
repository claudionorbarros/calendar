<?php

/**
	* this file is part of a calendar module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a calendar module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Calendar Module
 */


//messages
$lang['calendar:success']		=	'Het kalender item is toegevoegd';
$lang['calendar:error']			=	'Er zijn fouten opgetreden tijdens het toevoegen van het kalender item';
$lang['calendar:no_items']		=	'Geen Items';

//page titles

$lang['calendar:create']		=	'Creëer Item';
$lang['calendar:create_cat']    =   'Creëer Kategorie';


//labels
$lang['calendar:name']			=	'Naam';
$lang['calendar:date']			=	'Datum';
$lang['calendar:description']	=	'Beschrijving';
$lang['calendar:price']			=	'Prijs';
$lang['calendar:item_list']		=	'Item lijst';
$lang['calendar:view']			=	'Bekijken';
$lang['calendar:edit']			=	'Bewerken';
$lang['calendar:delete']		=	'Verwijderen';
$lang['calendar:published']		= 	'Gepubliceerd ?';
$lang['calendar:new_event']     =   'Nieuw event';
$lang['calendar:stoptime']      =   'Stopt';
$lang['calendar:starttime']     =   'Start';
$lang['calendar:stoptimefilter'] =   'Stopt (of vroeger)';
$lang['calendar:starttimefilter']=   'Start (of later)';
$lang['calendar:location']      =   'Locatie';
$lang['calendar:categorie']     =   'Categorie';
$lang['calendar:events']        =   'Events';
$lang['calendar:categories']    =   'Categorieën';
$lang['calendar:year']          =   'Jaar';
$lang['calendar:week']          =   'Week';
$lang['calendar:breadcrumb']    =   'Agenda';
$lang['calendar:duplicate']     =   "Dupliceer";


//buttons
$lang['calendar:custom_button']	=	'Custom Button';
$lang['calendar:cancel']        =   'Cancel';
$lang['calendar:items']			=	'Items';
$lang['calendar:age']           =   'Minimale leeftijd';

//currency
$lang['calendar:currency']	    =	'&euro;';

//categories ladida                                

$lang['calendar:color']         =   "Kleur";
$lang['calendar:is_published']  =   "Gepubliceerd";
$lang['calendar:non_published'] =   "Ongepubliceerd";

$lang['calendar:January']       =   "Januari";
$lang['calendar:February']      =   "Februari";
$lang['calendar:March']         =   "Maart";
$lang['calendar:April']         =   "April";
$lang['calendar:May']           =   "Mei";
$lang['calendar:June']          =   "Juni";
$lang['calendar:July']          =   "Juli";
$lang['calendar:August']        =   "Augustus";
$lang['calendar:September']     =   "September";
$lang['calendar:October']       =   "October";
$lang['calendar:November']      =   "November";
$lang['calendar:December']      =   "December";

$lang['calendar:Sunday']        =   "Zondag";
$lang['calendar:Monday']        =   "Maandag";
$lang['calendar:Tuesday']       =   "Dinsdag";
$lang['calendar:Wednesday']     =   "Woensdag";
$lang['calendar:Thursday']      =   "Donderdag";
$lang['calendar:Friday']        =   "Vrijdag";
$lang['calendar:Saturday']      =   "Zaterdag";

?>
