<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
	* this file is part of a calendar module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation, either version 3 of the License, or
	* (at your option) any later version.

	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.

	* You should have received a copy of the GNU General Public License
	* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * This is a calendar module for PyroCMS
 * 
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Calendar Module
 */
class Admin_categories extends Admin_Controller
{
	/**
	 * The current active section
	 *
	 * @var string
	 */
	protected $section = 'categories';

	/**
	 * Validation rules used by the form_validation library
	 *
	 * @var array
	 */
	private $validation_rules = array(
		array(
			'field' => 'name',
			'label' => 'Title',
			'rules' => 'trim|required|max_length[60]'
		),
		array(
			'field' => 'description',
			'label' => 'Description',
			'rules' => 'trim|max_length[200]'
		),
		array(
			'field' => 'color',
			'label' => 'Color',
			'rules' => 'trim|max_length[7]'
		)
	);

	/**
	 * Constructor method
	 */
	public function __construct()
	{
		// Call the parent's constructor
		parent::__construct();

		$this->load->model('calendar_categories_m');
		$this->lang->load('calendar');

		// Load the validation library
		$this->load->library('form_validation');

		// Set the validation rules
		$this->form_validation->set_rules($this->validation_rules);

		$this->template
		->append_js('jquery/jquery.ui.nestedSortable.js')
		->append_metadata($this->load->view('fragments/wysiwyg'));
	}


	/**
	 * main portion of admin the index the begin
	 * @return void
	 */
	public function index()
	{
		$data = new  stdClass();
		$data->categories = $this->calendar_categories_m->get_all();
		$this->template
			->title($this->module_details['name'], lang('calendar:iets'))
			->build('admin/categories/index', $data);
	}

	/**
	 * the past that creates an category
	 * @return void
	 */
	public function create()
	{
		if ($input = $this->input->post())
		{
			$data = $this->input->post();
			$data['color'] = substr($data['color'], 1);
			
			if ($id = $this->calendar_categories_m->create($data) && $this->form_validation->run())
			{
				$this->session->set_flashdata('success', lang('calendar.success'));
				redirect('admin/calendar/categories');
			}
			else
			{
				$this->session->set_flashdata('error', lang('calendar.error'));
				redirect('admin/calendar/categories/create');
			}
		}
		else
		{
			$data = new  stdClass();
			foreach ($this->validation_rules AS $rule)
			{
				
				$data->{$rule['field']} = $this->input->post($rule['field']);
			}
		}

		$this->pyrocache->delete('calendar_categories_m');
		$this->template->title($this->module_details['name'], lang('calendar.new_event'))
		->append_js('module::spectrum.js')
		->append_css('module::spectrum.css')
		->append_js('module::categories_admin.js')

		->build('admin/categories/form', $data);
	}

	/**
	 * edit an category
	 * @param  integer $id yes to edit an category you need an id of that category
	 * @return void
	 */
	public function edit($id = 0)
	{
		$data = $this->calendar_categories_m->get($id);
		$data->color = "#" . $data->color;

		if ($input = $this->input->post())
		{
			$data = $this->input->post();
			$data['color'] = substr($data['color'], 1);
			
			if ($this->calendar_categories_m->edit($id,$data) && $this->form_validation->run())
			{
				$this->session->set_flashdata('success', lang('calendar.success'));
				redirect('admin/calendar/categories');
			}
			else
			{
				$this->session->set_flashdata('error', lang('calendar.error'));
			}
		}

		$this->pyrocache->delete('calendar_categories_m');
		
		$this->template->title($this->module_details['name'], lang('calendar.edit'))

		->append_js('module::spectrum.js')
		->append_css('module::spectrum.css')
		->append_js('module::categories_admin.js')
		->build('admin/categories/form', $data);
	}

	/**
	 * delete an category
	 * @param  integer $id need an id for a singel category
	 * @return [type]      [description]
	 */
	public function delete($id = 0)
	{
		// make sure the button was clicked and that there is an array of ids
		if (isset($_POST['btnAction']) AND is_array($_POST['action_to']))
		{
			$this->calendar_categories_m->delete_many($this->input->post('action_to'));
		}
		elseif (is_numeric($id))
		{
			$this->calendar_categories_m->delete($id);
		}
		$this->pyrocache->delete('calendar_categories_m');
		redirect('admin/calendar/categories');
	}

}