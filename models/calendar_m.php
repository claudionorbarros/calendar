<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
	* this file is part of a calendar module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a calendar module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Calendar Module
 */
 
class Calendar_m extends MY_Model {
	
	public $event_validation_rules = array(
		array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|max_length[100]|required'
		),
		array(
			'field' => 'published',
			'label' => 'Published',
			'rules' => 'trim|max_length[1]'
		),
		array(
			'field' => 'start_on',
			'label' => 'Start on',
			'rules' => 'trim|max_length[100]|required|callback_compare_dates'
		),
		array(
			'field' => 'start_on_minute',
			'label' => 'Start on minute',
			'rules' => 'trim|max_length[100]|required'
		),
		array(
			'field' => 'start_on_hour',
			'label' => 'Start on hour',
			'rules' => 'trim|max_length[100]|required'
		),
		array(
			'field' => 'stop_on',
			'label' => 'Stop on',
			'rules' => 'trim|max_length[100]|required'
		),
		array(
			'field' => 'stop_on_minute',
			'label' => 'Stop on minute',
			'rules' => 'trim|max_length[100]|required'
		),
		array(
			'field' => 'stop_on_hour',
			'label' => 'Stop on hour',
			'rules' => 'trim|max_length[100]|required'
		),
		array(
			'field' => 'description',
			'label' => 'Description',
			'rules' => 'trim|max_length[1000]'
		),
		array(
			'field' => 'location',
			'label' => 'Location',
			'rules' => 'trim|max_length[100]'
		),
		array(
			'field' => 'categorie_id',
			'label' => 'Categorie',
			'rules' => 'trim|integer'
		),
		array(
			'field' => 'price',
			'label' => 'Price',
			'rules' => 'trim|callback_numeric_wcomma'
		)
	);





	public function __construct()
	{
		parent::__construct();
		$this->_table = 'calendar';
	}

	/**
	 * get by key value
	 * @param  string $key
	 * @param  string $val
	 * @return array the asoc array from codeigniter
	 */
	public function get_cal_by($key,$val)
	{
		$result = $this->db
		->where($key,$val)
		->get($this->_table)
		->result();
		if (isset($result))
		{
			return $result[0];
		}
		return NULL;
	}

	/**
	 * get manny events by the params that where given
	 * @param  array  $params the parameters that will be used to search
	 * @return array the asoc array from codeigniter
	 */
	public function get_many_by($params = array())
	{

		$this->db->select('*')->from($this->_table);
		if (!empty($params['category']))
		{
			if (is_numeric($params['category']))
				$this->db->where($this->_table . '.categorie_id', $params['category']);
		}

		if (!empty($params['status']))
		{
			if ($params['status'] != '0')
			{
				$this->db->where('published', (int)$params['status']-1);
			}
		}

		if (!empty($params['starttime']) && !empty($params['stoptime']))
		{
			$datetime1 = new DateTime($params['starttime']);
			$datetime2 = new DateTime($params['stoptime']);

			if ($datetime1 < $datetime2)
			{
				$this->db->where("starttime > '" . $params['starttime'] . "' AND stoptime < '" . $params['stoptime'] . "'", NULL, FALSE);
				// $this->db->or_where("starttime < '" . $params['starttime'] . "' AND stoptime > '" . $params['stoptime'] . "'", NULL, FALSE);
				// $this->db->or_where("starttime > '" . $params['starttime'] . "' AND stoptime > '" . $params['stoptime'] . "' AND starttime < '" . $params['stoptime'] . "'", NULL, FALSE);
				// $this->db->or_where("starttime < '" . $params['starttime'] . "' AND stoptime > '" . $params['starttime'] . "' AND stoptime < '" . $params['stoptime'] . "'", NULL, FALSE);
				
			}

		}
		elseif (!empty($params['starttime']) && empty($params['stoptime']))
		{
			$this->db->where('starttime >', $params['starttime']);
		}
		elseif (!empty($params['stoptime']) && empty($params['starttime']))
		{
			$this->db->where('stoptime <', $params['stoptime']);
		}

		$results = $this->db->get()->result();
		
		return $results;
	}
	

	/**
	 * more get all this time for year, month and day
	 * @param  Interger  $year 
	 * @param  Interger  $month 
	 * @param  Integer 	$day   
	 * @return array the asoc array from codeigniter
	 */
	public function get_all_cal($year,$month, $day = 0)
	{	
		
		$this->db->select($this->_table . '.*, calendar_categories.color');
		$this->db->from($this->_table);

		if ($year && $month && $day)
		{
			$firstday = DateTime::createFromFormat('Y-m-d', $year . "-" . $month . "-". $day);
			$lastday = DateTime::createFromFormat('Y-m-d', $year . "-" . $month . "-" . $day);
		$firstday->setTime(23, 59, 00);
		$lastday->setTime(00, 01, 00);
			$this->db->where('starttime <=', $firstday->format('Y-m-d H:i:s'));
			$this->db->where('stoptime >=', $lastday->format('Y-m-d H:i:s'));
		}
		elseif ($year && $month)
		{
			$firstday = DateTime::createFromFormat('Y-m-d', $year . "-" . $month . "-1");
			$lastday = DateTime::createFromFormat('Y-m-d', $year . "-" . $month . "-1");
			$firstday->modify('first day of this month');
			$lastday->modify('last day of this month');
			$firstday->setTime(00, 01, 00);
			$lastday->setTime(23, 59, 00);
			$this->db->where('starttime >=', $firstday->format('Y-m-d H:i:s'));
			$this->db->where('stoptime <=', $lastday->format('Y-m-d H:i:s'));
		}
		else
		{
			$firstday = new DateTime();
			$lastday = new DateTime();
		}


		

		$this->db->where('published', '1');
		$this->db->join('calendar_categories', 'calendar_categories.id = ' . $this->_table . '.categorie_id','LEFT');

		$results = $this->db->get()->result();
		return $results;
	}	
	
	/**
	 * more get all this time for year and week
	 * @param  Interger $year
	 * @param  Interger $week
	 * @return array the asoc array from codeigniter
	 */
	public function get_all_cal_week($year,$week)
	{	
		$lastday = new DateTime();
		$firstday = new DateTime();
		
		if ($year && $week)
		{
			$lastday->setISODate($year, $week , 1);
			$firstday->setISODate($year, $week, 1);
		}

		$firstday->modify('last Sunday');
		$lastday->modify('next Saturday');
		$firstday->setTime(00, 01, 00);
		$lastday->setTime(23, 59, 00);

		$results = $this->db
		->select($this->_table . '.*, calendar_categories.color')
		->from($this->_table)
		->where('starttime >=', $firstday->format('Y-m-d'))
		->where('starttime <=', $lastday->format('Y-m-d'))
		->where('published', '1')
		->join('calendar_categories', 'calendar_categories.id = ' . $this->_table . '.categorie_id','LEFT')
		->get()
		->result();
		return $results;
	}	

	/**
	 * get all the events for the admin side
	 * @return array the asoc array from codeigniter
	 */
	public function get_all_cal_admin()
	{
		$results = $this->db
		->get($this->_table)
		->result();
		return $results;
	}	


	/**
	 * edit an event
	 * @param  Integer $id event id to edit
	 * @param  array $input the input array of the values to edit
	 * @return bool did it work
	 */
	public function edit($id, $input)
	{
		$this->db->trans_start();

		// validate the data and update
		$result = $this->update($id, array(
			'name'				=> $input['name'],
			'starttime'			=> $input['starttime'],
			'published'			=> ! empty($input['published']),
			'stoptime'			=> $input['stoptime'],
			'location'			=> $input['location'],
			'description'		=> $input['description'],
			'categorie_id'		=> $input['categorie_id'],
			'price'				=> $input['price']
		));

		// did it pass validation?
		if ( ! $result) return false;
		$input['id'] = $id;
		$this->db->trans_complete();
		return (bool) $this->db->trans_status();
	}

	/**
	 * create an event
	 * @param  array $input  an array of values that will make an event
	 * @return bool did it work
	 */
	public function create($input)
	{
		$this->db->trans_start();
		// validate the data and update
		$result = $this->insert(array(
			'name'				=> $input['name'],
			'starttime'			=> $input['starttime'],
			'published'			=> ! empty($input['published']),
			'stoptime'			=> $input['stoptime'],
			'location'			=> $input['location'],
			'description'		=> $input['description'],
			'price'				=> $input['price'],
			'categorie_id'		=> $input['categorie_id']
		));

		// did it pass validation?
		if ( ! $result) return false;
		$this->db->trans_complete();
		return (bool) $this->db->trans_status();
	}


	public function duplicate($input)
	{
		unset($input->id); 
		$result = $this->insert($input);

		return $result;
	}

	/**
	 * publith an event with id x
	 * @param  integer $id the id of the event to publish
	 * @return bool did it work out
	 */
	public function publish($id = 0)
	{
		return parent::update($id, array('published' => 1));
	}
}
