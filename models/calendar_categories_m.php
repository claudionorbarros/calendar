<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
	* this file is part of a calendar module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a calendar module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Calendar Module
 */
 
class Calendar_categories_m extends MY_Model {
	
	/**
	 * Constructor method
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{		
		parent::__construct();
		$this->_table = 'calendar_categories';
	}

	/**
	 * get all the categories
	 * @return array the asoc array from codeigniter
	 */
	public function get_all_cal()
	{
		return $this->db->get($this->_table)->result();
	}
	
	/**
	 * get all the names of the categories, in this case get all
	 * @return array the asoc array from codeigniter
	 */
	public function get_all_names()
	{
		$this->db->select('*')->from($this->_table);
		return $this->db->get()->result_array();
	}


	/**
	 * Insert a new category into the database
	 * 
	 * @param array $input The data to insert
	 * @return string
	 */
	public function create($input = array())
	{
		$this->db->trans_start();
		$result = $this->insert(array(
			'name'=>$input['name'],
			'description'=>url_title(strtolower(convert_accented_characters($input['description']))),
			'color'	=> $input['color']
		));
		// did it pass validation?
		if ( ! $result) return false;
		$this->db->trans_complete();
		return (bool) $this->db->trans_status();
	}

	/**
	 * Update an existing category
	 * 
	 * @param int $id The ID of the category
	 * @param array $input The data to update
	 * @return bool
	 */
	public function edit($id, $input)
	{
		return parent::update($id, array(
			'name'	=> $input['name'],
		    'description'	=> url_title(strtolower(convert_accented_characters($input['description']))),
		    'color'	=> $input['color']
		));
	}
}
