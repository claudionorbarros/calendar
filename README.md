# Calendar for PyroCMS

## Legal

This module was originally written by [Aat Karelse](http://vuurrosmedia.nl)
It is licensed under the version 3 of the GNU General Public License. [GPL license](http://www.gnu.org/licenses/)
More information about this module can be found at [more information](http://modules.vuurrosmedia.nl)

---

## Overview

Calendar is a backend, frontend module for PyroCMS and it supports version 2.1.x 
It is a calendar with events and event categories

---

## Features

- event date and time
- event categories with names and colors
- relative clean programming
- price options per event

---

## Widgets

Widget for a current week calendar

---

## Settings

None

---

## Installation

Download the archive and upload via Control Panel or Download the module from bitbucket.
Install the module.

---

## Credits

- Jerel Unruh for making available the sample module.
- The Pyro Dev team for making PyroCMS
- Victor Michnowicz for a php version check function
- Peter Nijssen for for a great calendar lib to work from.

---

## ToDo
+ the admin side could be more user frendly, not refilling event fields in case of wrong event adition
+ the second date picker is not working yet, annymore.
+ the price for the admin site should be optional
+ some sort of better location field for location would be nice
+ the last part of the code is somewhat rushed so not the most beatifull
+ translations into some more langs would be grand
+ also the translations tags are not present in all of the templates, thats a thing.
