<table width="100%" align="center" cellpadding="0" cellspacing="0" class="cal_table">
    <thead>
        <tr>
            <th colspan="2" class="previous"><<<a href=" {{ previous_link }}"> <div class="prefimage"></div> {{prefmonth }} </a></th>
            <th colspan="3" class="current">{{ month }} {{year }}</th>
            <th colspan="2" class="next"><a href="{{ next_link }}">{{ nextmont }}<div class="nextimage"></div> </a>>></th>
        </tr>
        <tr>
{{daysofweek}}
    <th>
        {{name}}
    </th>
{{/daysofweek}}
    </tr>
    </thead>
    <tbody>
    {{ weeks }}
    <tr>
        {{ days }}
        <td valign="top" style="height:80px;width:80px;" class="cal_td_days {{ today }}">         
            <span><a href="{{predaylink}}/{{year}}/{{month}}/{{ day }}">{{ day }}</a></span>
            <div>
                {{events}}
                        <span><a style="border-left:8px solid #{{catcolor}}" href="{{link}}"> {{ name }}</a></span>
                {{/events}}
            </div>
        </td>
        {{ /days }}
    </tr>
    {{ /weeks }}
    </tbody>
</table>
<div class="legenda">
<ul>
{{legends}}
<li style="border-left:8px solid #{{color}}">{{name}}</li>
{{/legends}}
</ul>
</div>
